package jp.alhinc.takizawa_yuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class CalclateSales {
	public static void main(String[]args){
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}
		HashMap<String , String> definisionMap = new HashMap<String , String>();
		HashMap<String , Long > calclateMap = new HashMap < String , Long >();
		BufferedReader br = null;
		try{
			File definisionFile = new File(args[0],"branch.lst");
			if (!definisionFile.exists()){
				System.out.println("支店定義ファイルが存在しません。");
				return;
			}

			FileReader fr = new FileReader(definisionFile);
			br = new BufferedReader(fr);
			String definision;


			while((definision = br.readLine()) != null) {

				String[] items = definision.split(",");
				definisionMap.put(items[0], items[1]);
				if(!items[0].matches("^[0-9]{3}")){
					System.out.println("支店定義ファイルのフォーマットが不正です。");
					return;
				}
				int elementNumber = definision.indexOf(",");
				int elementNumber2 = items[1].indexOf(",");
				if(elementNumber != 3 && elementNumber2 != -1){
					System.out.println("支店定義ファイルのフォーマットが不正です。");
					return;
				}
				long zeloValue = 0 ;
				calclateMap.put(items[0], zeloValue);
			}

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました。");
		}finally{
			if(br != null){
				try{
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました。");
				}
			}
		}


		File dir1 = new File(args[0]);
		String [] calclatefileList = dir1.list();

		ArrayList<String> calclateSet = new ArrayList<String>();
		for( int i = 0 ; i < calclatefileList.length ; i++){
			if(calclatefileList[i].matches("^[0-9]{8}.rcd$")){
				calclateSet.add(calclatefileList[i]);
			}
		}
		for(int i = 0; i < ((calclateSet.size()) - 1); i++){
			String numberCheck1 = calclateSet.get(i).substring(0, 8);
			String numberCheck2 = calclateSet.get(i + 1).substring(0, 8);
			int sub = Integer.parseInt(numberCheck2) - Integer.parseInt(numberCheck1) ;
			if(sub != 1){
				System.out.println("売上ファイル名が連番になっていません。");
				return;
			}
		}
		for(int i = 0 ; i < calclateSet.size(); i++){
			String calclatePass = calclateSet.get(i);

			try{
				File calclateFile = new File(args[0],calclatePass);
				FileReader fr = new FileReader(calclateFile);
				br = new BufferedReader(fr);
				String calclate;
				ArrayList <String>calclateBox = new ArrayList<String>();

				while((calclate = br.readLine()) != null) {
					calclateBox.add(calclate);

				}
				int lineCheck = calclateBox.size();
				if(lineCheck != 2){
					System.out.println("<" + calclateSet.get(i) + ">のフォーマットが不正です。");
					return;
				}
				long calclateValue = calclateMap.get(calclateBox.get(0));
				String codeCheck = calclateBox.get(0);

				if(!calclateMap.containsKey(codeCheck)){
					System.out.println("< " +calclateSet.get(i) + ">の支店コードが不正です。");
					return;
				}
				Long longCalclateValue = new Long(0);

				try{
				longCalclateValue = Long.parseLong(calclateBox.get(1));
				}catch(NumberFormatException e){
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}
				long sumCalclateValue = 0 ;
				sumCalclateValue = longCalclateValue + calclateValue ;
				int sumCheck = String.valueOf(sumCalclateValue).length();
					if(sumCheck >10 ){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				calclateMap.put(calclateBox.get(0), sumCalclateValue);


			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました。");
			}finally{
				if(br != null){
					try{
						br.close();
					}catch(IOException e){
						System.out.println("予期せぬエラーが発生しました。");
					}
				}
			}
		}
		BufferedWriter bw =null;
		try{
			File aggregateFile = new File (args [0] , "branch.out");
			FileWriter fw = new FileWriter (aggregateFile);
			bw = new BufferedWriter (fw);

			for(Map.Entry<String , String > entry : definisionMap.entrySet()){
				String shopCode = entry.getKey();
				String shop = entry.getValue();
				long sumCalclate = calclateMap.get(entry.getKey());
				try{
					bw.write(shopCode + "," + shop + "," + sumCalclate);
					bw.newLine();
				}catch(Exception e){
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}
			}

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました。");

		}finally{
			if(bw != null){
				try{
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました。");

				}
			}
		}
	}
}
